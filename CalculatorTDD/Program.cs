﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalculatorTDD;

namespace CalculatorTDD
{
    class Program
    {
        static void Main(string[] args)
        {
            //ArgumentValidator validador = new ArgumentValidator(-100, 100, -200, 200);

            Calculator calculator = new Calculator(-100, 100, -200, 200);
            int resultado = calculator.Add(1, 99);
            Console.WriteLine("El resultado es: {0}", resultado);
            Console.WriteLine("");
        }
    }
}
