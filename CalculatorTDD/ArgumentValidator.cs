﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorTDD
{


    public interface IComprobarLimites
    {
        void CompruebaLimitesArgumentos(Calculator calc,int minArg1, int MaxArg2);
        void CompruebaLimitesResultados(Calculator calc, int resultado);

    }

    public class ArgumentValidator : IComprobarLimites
    {
        

        #region Constructores

        
        #endregion

        public void CompruebaLimitesArgumentos(Calculator calculadora, int arg1, int arg2)
        {
            if ((arg1 > calculadora.MaxValue) || (arg2 > calculadora.MaxValue))
            {
                throw new OverflowException(@"[Uno de los argumentos se pasa el límite por arriba:" + arg1 + ", " + arg2);
            }
            if ((arg1 < calculadora.MinValue) || (arg2 < calculadora.MinValue))
            {
                throw new OverflowException(@"[Uno de los argumentos se pasa el límite por abajo]");
            }
        }

        public void CompruebaLimitesResultados(Calculator calculadora, int resultado)
        {
            if (resultado < calculadora.LimitAbajo)
            {
                throw new OverflowException(@"[El resultado final se pasa el límite por abajo:" + resultado);
            }
            if (resultado > calculadora.LimitArriba)
            {
                throw new OverflowException(@"[El resultado final se pasa el límite por arriba:" + resultado);
            }
        }
    }
}
