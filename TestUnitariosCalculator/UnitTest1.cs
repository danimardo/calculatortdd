// unit test code
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorTDD;
using Rhino.Mocks;
using Moq;

namespace TestUnitarios
{
    [TestClass]
    public class BankAccountTests
    {

        static Calculator calculator;
        static ArgumentValidator validador;

        [ClassInitialize()]
        public static void SetUp(TestContext context)
        {
            validador = new ArgumentValidator();
            calculator = new Calculator(-100, 100, -200, 200);
        }
        


        [TestMethod]
        public void Add()
        {
            int result = calculator.Add(2, 7);
            Assert.AreEqual(result, 9);
        }

        [TestMethod]
        public void Sustract()
        {
            int result = calculator.Sustract(7, 2);
            Assert.AreEqual(result, 5);
        }

        [TestMethod]
        public void SustractNegative()
        {
            int result = calculator.Sustract(2, 7);
            Assert.AreEqual(result, -5);
        }

        [TestMethod]
        public void RestarA�adiendoValoresLimite()
        {
            try
            {
                int result = calculator.Sustract(2, 150);
                Assert.Fail("Excepci�n de salida de l�mite por abajo no manejada");
            }
            catch (OverflowException)
            {
                // Si esta manejada no le metemos el Assert.Fail y por lo tanto est� correcto.
            }

        }

        [TestMethod]
        public void EstablecerLimitesACalculator()
        {
            Assert.AreEqual(calculator.MinValue, -100);
            Assert.AreEqual(calculator.MaxValue, 100);
        }

        [TestMethod]
        public void NosPasamosElLimitePorArribaAdd()
        {
            try
            {
                int result = calculator.Add(100, 101);
                Assert.Fail("Excepci�n de salida de l�mite por arriba no manejada");
            }
            catch (OverflowException)
            {
                // Si esta manejada no le metemos el Assert.Fail y por lo tanto est� correcto.
            }
        }

        [TestMethod]
        public void SumamosPeroConUnValorMinimoSuperandoElMinimo()
        {
            try
            {
                int result = calculator.Add(-150, 1);
                Assert.Fail("Excepci�n de salida de l�mite por debajo no manejada");
            }
            catch (OverflowException)
            {
                // Si esta manejada no le metemos el Assert.Fail y por lo tanto est� correcto.
            }
        }

        [TestMethod]
        public void SumamosPeroConvaloresSuperandoElMaximo()
        {
            try
            {
                int result = calculator.Add(100, 101);
                Assert.Fail("Excepci�n de salida Superando el maximo");
            }
            catch (OverflowException)
            {
                // Si esta manejada no le metemos el Assert.Fail y por lo tanto est� correcto.
            }
        }

        // limites restas

        [TestMethod]
        public void RestamosPeroConUnValorMinimoSuperandoElMinimo()
        {
            try
            {
                int result = calculator.Sustract(-150, 1);
                Assert.Fail("Excepci�n de salida de l�mite por debajo no manejada");
            }
            catch (OverflowException)
            {
                // Si esta manejada no le metemos el Assert.Fail y por lo tanto est� correcto.
            }
        }

        [TestMethod]
        public void RestamosPeroConvaloresSuperandoElMaximo()
        {
            try
            {
                int result = calculator.Sustract(100, 101);
                Assert.Fail("Uno de los parametros de supera el maximo");
            }
            catch (OverflowException)
            {
                // Si esta manejada no le metemos el Assert.Fail y por lo tanto est� correcto.
            }
        }

        [TestMethod]
        public void ComprobamosElmetodoLimiteDeArgumentosConMaximo()
        {
            try
            {
                validador.CompruebaLimitesArgumentos(calculator ,101, 100);
                Assert.Fail("Uno de los parametros de supera el maximo");
            }
            catch (OverflowException)
            {
                // Si esta manejada no le metemos el Assert.Fail y por lo tanto est� correcto.
            }
        }

        public void ComprobamosElmetodoLimiteDeArgumentosConMinimo()
        {
            try
            {
                validador.CompruebaLimitesArgumentos(calculator, -100, 100);
                Assert.Fail("Uno de los parametros de supera el minimo");
            }
            catch (OverflowException)
            {
                // Si esta manejada no le metemos el Assert.Fail y por lo tanto est� correcto.
            }
        }

        // Comprobamos que los metodos validan los argumentos antes de hacer otra cosa.

        [TestMethod]
        public void PruebaMockDeLlamadaAComprobadorDeArgumentos()
        {
            IComprobarLimites OperacionMock =
              MockRepository.GenerateStrictMock<IComprobarLimites>();

            OperacionMock.Expect(
             a => a.CompruebaLimitesArgumentos(calculator ,99,99));


            calculator.validador = OperacionMock;
            int result = calculator.Add(99, 99);


            OperacionMock.VerifyAllExpectations();


        }

        
           
        


    }


}